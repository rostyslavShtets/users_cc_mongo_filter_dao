package api

import (
	"users_cc_mongo_filter_dao/dao"
	"users_cc_mongo_filter_dao/config"
)

var configDB = config.Config{}
var daoDB = dao.DAO{}

// Parse the configuration file 'config.toml', and establish a connection to DB
func InitDB() {
	configDB.Read()

	daoDB.Server = configDB.Server
	daoDB.Database = configDB.Database
	daoDB.Connect()
}

// func DaoAccess() *dao.DAO {
// 	return &daoDB
// }
