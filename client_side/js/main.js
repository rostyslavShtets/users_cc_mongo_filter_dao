'use strict';

const API_FOR_STRING = "http://localhost:8084/api/people?name=";
const API_FOR_OBJECT = "http://localhost:8084/api_object/people?filter=";

//object with strings for testing filters like string
let queryString = {
  twoNames: "Tom&name=Sandra&name=John",
  twoNamesAndAge: "Tom&name=Sandra&name=John&age=85",
  twoNamesLimitSkip: "Tom&name=Sandra&name=John&limit=10&skip=1"
}

//object for testing filters when we pass object, then encode to json and then to string
// on server side
let objForFilter = {
  firstName: "Tom",
  lastName: "Mongo",
  age: 49
};

//to find element in DOM
let buttonTestStringFilter = document.getElementById("filterString");
let buttonTestObjectFilter = document.getElementById("filterObject");

//to handle events
buttonTestStringFilter.addEventListener("click", () => sendGetRequest(API_FOR_STRING, queryString.twoNames, false));
buttonTestObjectFilter.addEventListener("click", () => sendGetRequest(API_FOR_OBJECT, objForFilter, true));

// func for making get requests to server
function sendGetRequest(api, urlString, itIsForFilterObject) {
  if (itIsForFilterObject) {
    console.log(urlString);
    let objToString = encodeURIComponent(JSON.stringify(urlString))
    urlString = objToString
    console.log(objToString);
  }
  // variable fo url
  let fullUrl = api + urlString;
  // console.log("url:", fullUrl)

  fetch(fullUrl)
  .then(function(response) {
    console.log(response.headers.get('Content-Type')); // application/json; charset=utf-8
    console.log(response.status); // 200
    // console.log(response)
    return response.json();
   })
  .then(function(user) {
    console.dir(user); // iliakan
  })
  .catch( alert );
}


// console.dir(obj);
// console.dir(objToString);
// console.dir('http://localhost:8081/api_object/people?filter=' + objToString);

// fetch('http://localhost:8081/api_object/people?filter=' + objToString)
// http://localhost:8081/api/people?name=Tom&name=Sandra&name=John


  // // 1. Создаём новый объект XMLHttpRequest
// var xhr = new XMLHttpRequest();

// // 2. Конфигурируем его: GET-запрос на URL 'phones.json'
// xhr.open('GET', 'localhost:8084/people', false);

// // 3. Отсылаем запрос
// xhr.send();

// // 4. Если код ответа сервера не 200, то это ошибка
// if (xhr.status != 200) {
//   // обработать ошибку
//   alert( xhr.status + ': ' + xhr.statusText ); // пример вывода: 404: Not Found
// } else {
//   // вывести результат
//   console.log( xhr.responseText ); // responseText -- текст ответа.
// }
