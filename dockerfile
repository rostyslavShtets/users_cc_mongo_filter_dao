# FROM golang:latest
# RUN mkdir /app
# ADD . /app/
# WORKDIR /app
# RUN go build -o main .
# EXPOSE 80
# CMD ["/app/main"]


FROM bitnami/minideb
ADD main /
ADD config.toml /
EXPOSE 80
CMD ["/main"]
