package main

import (
	"users_cc_mongo_filter_dao/api"
	"users_cc_mongo_filter_dao/utils"
	"github.com/go-chi/chi"
	"fmt"
	"log"
	"net/http"
)
const port = ":8081"

func main() {
	// additional internal check 
	utils.Hello()
	fmt.Println("new_new")

	// init DB
	api.InitDB()
			
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(".root"))
	})

	//request for people
	r.Get("/people", api.AllPeopleEndPoint)
	r.Get("/people/{personID}",  api.FindPersonEndPoint)
	r.Get("/api/people", api.GetPeopleFilter)
	r.Get("/api_object/people", api.GetPeopleFilterObject)
	r.Post("/people", api.CreatePersonEndPoint)
	r.Put("/people/{personID}", api.UpdatePersonEndPoint)
	r.Delete("/people/{personID}", api.DeletePersonEndPoint)
	
	//request for books
	r.Get("/books", api.AllBooksEndPoint)
	r.Get("/books/{bookID}", api.FindBookEndPoint)
	r.Post("/books", api.CreateBookEndPoint)
	r.Delete("/books/{bookID}", api.UpdateBookEndPoint)
	r.Put("/books/{bookID}", api.DeleteBookEndPoint)


	fmt.Printf("Serv on port %v....\n", port)
	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}
	// log.Fatal(http.ListenAndServe(port, r))
}

